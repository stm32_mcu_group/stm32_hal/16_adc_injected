//
// Created by Csurleny on 5/1/2022.
//

#include <string.h>
#include "main.h"
#include "ssd1306_oled.h"

#define ABS(X)  ((X) > 0 ? (X) : -(X))

// The I2C function handle defined in main.c
extern I2C_HandleTypeDef hi2c1;

static inline void ssd1306_WriteCommand(uint8_t command){
    HAL_I2C_Mem_Write(&hi2c1, SSD1306_I2C_ADDR, 0x00, 1, &command, 1, HAL_MAX_DELAY);
}

static inline void ssd1306_WriteData(uint8_t data){
    HAL_I2C_Mem_Write(&hi2c1, SSD1306_I2C_ADDR, 0x40, 1, &data, 1, HAL_MAX_DELAY);
}

static uint8_t  SSD1306_Buffer[SSD1306_WIDTH * SSD1306_HEIGHT / 8];

typedef struct{
    uint16_t currentX;
    uint16_t currentY;
    uint8_t inverted;
    uint8_t initialized;
} SSD1306_t;

static SSD1306_t SSD1306;

uint8_t SSD1306_Init(){

    if(HAL_I2C_IsDeviceReady(&hi2c1, SSD1306_I2C_ADDR, 3, 5)!= HAL_OK)
        return 0;

    ssd1306_WriteCommand(0xAE);     // Display OFF
    ssd1306_WriteCommand(0x20);     // Memory addressing mode
    ssd1306_WriteCommand(0x10);     // 00 - Horizontal, 01 - Vertical, 10 -  Page (Reset), 11 - Invalid
    ssd1306_WriteCommand(0xB0);     // set page start for Page Addressing mode, 0 - 7
    ssd1306_WriteCommand(0xC8);     // set output scan direction
    ssd1306_WriteCommand(0x00);     // set low column address
    ssd1306_WriteCommand(0x10);     // set high column address
    ssd1306_WriteCommand(0x40);     // set start line address
    ssd1306_WriteCommand(0x81);     // set contrast control
    ssd1306_WriteCommand(0xFF);
    ssd1306_WriteCommand(0xA1);     // set segment remap 0 to 127
    ssd1306_WriteCommand(0xA6);     // set normal display
    ssd1306_WriteCommand(0xA8);     // set multiplex ratio 1 to 64
    ssd1306_WriteCommand(0x3F);
    ssd1306_WriteCommand(0xA4);     // A4h - output follows RAM content, A5h output ignores RAM content
    ssd1306_WriteCommand(0xD3);     // set display offset
    ssd1306_WriteCommand(0x00);     // no offset
    ssd1306_WriteCommand(0xD5);     // set display clock frequency
    ssd1306_WriteCommand(0xF0);     // set divide ratio
    ssd1306_WriteCommand(0xD9);     // set pre-charge period
    ssd1306_WriteCommand(0x22);
    ssd1306_WriteCommand(0xDA);     // set com pins hardware configuration
    ssd1306_WriteCommand(0x12);
    ssd1306_WriteCommand(0xDB);     // set VcomH
    ssd1306_WriteCommand(0x20);     // 20h - 0.77 x Vcc
    ssd1306_WriteCommand(0x8D);     // enable DC-DC
    ssd1306_WriteCommand(0x14);
    ssd1306_WriteCommand(0xAF);     // Display ON

    // Clear screen
    SSD1306_Fill(SSD1306_COLOR_BLACK);

    // Update screen
    SSD1306_UpdateScreen();

    // Initialize default values of current position/state
    SSD1306.currentX = 0;
    SSD1306.currentY = 0;
    SSD1306.inverted = 0;
    SSD1306.initialized = 1;

    return 1;
}

void SSD1306_UpdateScreen(){
    uint8_t m;
    for(m = 0; m < 8; ++m){
        ssd1306_WriteCommand(0xB0 + m);
        ssd1306_WriteCommand(0x00);
        ssd1306_WriteCommand(0x10);
        HAL_I2C_Mem_Write(&hi2c1, SSD1306_I2C_ADDR, 0x40, 1,
                          &SSD1306_Buffer[SSD1306_WIDTH*m], SSD1306_WIDTH, HAL_MAX_DELAY);
    }
}

void SSD1306_ToggleInvert(){
    uint16_t  i;
    SSD1306.inverted = !SSD1306.inverted;
    for (i = 0; i < sizeof(SSD1306_Buffer); i++) {
        SSD1306_Buffer[i] = ~SSD1306_Buffer[i];
    }
}

void SSD1306_Fill(SSD1306_COLOUR_t colour){
    memset(SSD1306_Buffer, (colour == SSD1306_COLOR_BLACK ? 0x00 : 0xFF), sizeof(SSD1306_Buffer));
}

void SSD1306_GotoXY(uint16_t x, uint16_t y){
    SSD1306.currentX = x;
    SSD1306.currentY = y;
}

void SSD1306_DrawPixel(uint16_t x, uint16_t y, SSD1306_COLOUR_t colour){

    if(x >= SSD1306_WIDTH || y >= SSD1306_HEIGHT)
        return;
    if(SSD1306.inverted)
        colour = (SSD1306_COLOUR_t)!colour;

    if(colour == SSD1306_COLOR_WHITE)
        SSD1306_Buffer[x + (y / 8) * SSD1306_WIDTH] |= 1 << (y % 8);
    else
        SSD1306_Buffer[x + (y / 8) * SSD1306_WIDTH] &= ~(1 << (y % 8));

}

char SSD1306_Putc(char ch, fDef_t* Font, SSD1306_COLOUR_t colour){
    uint32_t i, j, b;

    if(SSD1306_WIDTH <= (SSD1306.currentX + Font->fWidth) ||SSD1306_HEIGHT <= (SSD1306.currentY + Font->fHeight))
        return 0;
    for(i = 0; i < Font->fHeight; ++i){
        b = Font->fData[(ch - 32) * Font->fHeight+i];
        for(j = 0; j < Font->fWidth; ++j )
            if((b<<j) & 0x8000)
                SSD1306_DrawPixel(SSD1306.currentX + j, SSD1306.currentY + i, colour);
            else
                SSD1306_DrawPixel(SSD1306.currentX + j, SSD1306.currentY + i, (SSD1306_COLOUR_t)!colour);
    }
    SSD1306.currentX += Font->fWidth;

    return ch;
}

char SSD1306_Puts(char* str, fDef_t* Font, SSD1306_COLOUR_t colour){
    while(*str){
        if(SSD1306_Putc(*str, Font, colour) != *str)
            return *str;
        str++;
    }
    return *str;
}

void SSD1306_DrawLine(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, SSD1306_COLOUR_t colour){
    int16_t dx, dy, sx, sy, err, e2, i, tmp;

    if(x0 >= SSD1306_WIDTH)
        x0 = SSD1306_WIDTH - 1;
    if(x1 >= SSD1306_WIDTH)
        x1 = SSD1306_WIDTH - 1;
    if(y0 >= SSD1306_HEIGHT)
        y0 = SSD1306_HEIGHT - 1;
    if(y1 >= SSD1306_HEIGHT)
        y1 = SSD1306_HEIGHT - 1;

    dx = (x0 < x1) ? (x1 - x0) : (x0 - x1);
    dy = (y0 < y1) ? (y1 - y0) : (y0 - y1);
    sx = (x0 < x1) ? 1 : -1;
    sy = (y0 < y1) ? 1 : -1;
    err = ((dx > dy) ? dx : -dy) / 2;

    if(dx == 0){
        if(y1 < y0){
            tmp = y1;
            y1 = y0;
            y0 = tmp;
        }
        if(x1 < x0){
            tmp = x1;
            x1 = x0;
            x0 = tmp;
        }
        for(i = y0; i <= y1; ++i)
            SSD1306_DrawPixel(x0, i, colour);
        return;
    }

    if(dy == 0){
        if(y1 < y0){
            tmp = y1;
            y1 = y0;
            y0 = tmp;
        }
        if(x1 < x0){
            tmp = x1;
            x1 = x0;
            x0 = tmp;
        }
        for(i = x0; i <= x1; ++i)
            SSD1306_DrawPixel(i, y0, colour);
        return;
    }

    do{
        SSD1306_DrawPixel(x0, y0, colour);
        e2 = err;
        if(e2 > -dx){
            err -= dy;
            x0 += sx;
        }
        if(e2 < dy){
            err += dx;
            y0 += sy;
        }
    } while (x0 != x1 | y0 != y1);

}

void SSD1306_DrawRectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, SSD1306_COLOUR_t colour){
    if(x >= SSD1306_WIDTH || y >= SSD1306_HEIGHT)
        return;
    if((x + w) >= SSD1306_WIDTH)
        w = SSD1306_WIDTH - x;
    if((y + h) >= SSD1306_HEIGHT)
        h = SSD1306_HEIGHT - y;

    SSD1306_DrawLine(x, y, x + w, y, colour);
    SSD1306_DrawLine(x, y + h, x + w, y + h, colour);
    SSD1306_DrawLine(x, y, x, y + h, colour);
    SSD1306_DrawLine(x + w, y, x + w, y + h, colour);

}
void SSD1306_DrawFilledRectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, SSD1306_COLOUR_t colour){
    uint8_t i;

    if(x >= SSD1306_WIDTH || y >= SSD1306_HEIGHT)
        return;
    if((x + w) >= SSD1306_WIDTH)
        w = SSD1306_WIDTH - x;
    if((y + h) >= SSD1306_HEIGHT)
        h = SSD1306_HEIGHT - y;

    for(i = 0; i <= h; ++i)
        SSD1306_DrawLine(x, y + i, x + w, y + i, colour);
}

void SSD1306_DrawTriangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t x3, uint16_t y3, SSD1306_COLOUR_t colour){
    SSD1306_DrawLine(x1, y1, x2, y2, colour);
    SSD1306_DrawLine(x2, y2, x3, y3, colour);
    SSD1306_DrawLine(x3, y3, x1, y1, colour);
}

void SSD1306_DrawFilledTriangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t x3, uint16_t y3, SSD1306_COLOUR_t colour){
    int16_t deltax = 0, deltay = 0, x = 0, y = 0, xinc1 = 0, xinc2 = 0, yinc1 = 0, yinc2 = 0, den = 0,
    num = 0, numadd = 0, numpixels = 0, curpixel = 0;

    deltax = ABS(x2 - x1);
    deltay = ABS(y2 - y1);
    x = x1;
    y = y1;

    if (x2 >= x1) {
        xinc1 = 1;
        xinc2 = 1;
    } else {
        xinc1 = -1;
        xinc2 = -1;
    }

    if (y2 >= y1) {
        yinc1 = 1;
        yinc2 = 1;
    } else {
        yinc1 = -1;
        yinc2 = -1;
    }

    if (deltax >= deltay){
        xinc1 = 0;
        yinc2 = 0;
        den = deltax;
        num = deltax / 2;
        numadd = deltay;
        numpixels = deltax;
    } else {
        xinc2 = 0;
        yinc1 = 0;
        den = deltay;
        num = deltay / 2;
        numadd = deltax;
        numpixels = deltay;
    }

    for(curpixel = 0; curpixel <= numpixels; ++curpixel){
        SSD1306_DrawLine(x, y, x3, y3, colour);

        num += numadd;
        if(num >= den){
            num -= den;
            x += xinc1;
            y += yinc1;
        }
        x += xinc2;
        y += yinc2;
    }
}

void SSD1306_DrawCircle(int16_t x0, int16_t y0, int16_t r, SSD1306_COLOUR_t colour){
    int16_t f = 1 - r;
    int16_t ddF_x = 1;
    int16_t ddF_y = -2 * r;
    int16_t x = 0;
    int16_t y = r;

    SSD1306_DrawPixel(x0, y0 + r, colour);
    SSD1306_DrawPixel(x0, y0 - r, colour);
    SSD1306_DrawPixel(x0 + r, y0, colour);
    SSD1306_DrawPixel(x0 - r, y0, colour);

    while (x < y) {
        if (f >= 0) {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x;

        SSD1306_DrawPixel(x0 + x, y0 + y, colour);
        SSD1306_DrawPixel(x0 - x, y0 + y, colour);
        SSD1306_DrawPixel(x0 + x, y0 - y, colour);
        SSD1306_DrawPixel(x0 - x, y0 - y, colour);

        SSD1306_DrawPixel(x0 + y, y0 + x, colour);
        SSD1306_DrawPixel(x0 - y, y0 + x, colour);
        SSD1306_DrawPixel(x0 + y, y0 - x, colour);
        SSD1306_DrawPixel(x0 - y, y0 - x, colour);
    }
}

void SSD1306_DrawFilledCircle(int16_t x0, int16_t y0, int16_t r, SSD1306_COLOUR_t colour){
    int16_t f = 1 - r;
    int16_t ddF_x = 1;
    int16_t ddF_y = -2 * r;
    int16_t x = 0;
    int16_t y = r;

    SSD1306_DrawPixel(x0, y0 + r, colour);
    SSD1306_DrawPixel(x0, y0 - r, colour);
    SSD1306_DrawPixel(x0 + r, y0, colour);
    SSD1306_DrawPixel(x0 - r, y0, colour);
    SSD1306_DrawLine(x0 - r, y0, x0 + r, y0, colour);

    while (x < y) {
        if (f >= 0) {
            y--;
            ddF_y += 2;
            f += ddF_y;
        }
        x++;
        ddF_x += 2;
        f += ddF_x;

        SSD1306_DrawLine(x0 - x, y0 + y, x0 + x, y0 + y, colour);
        SSD1306_DrawLine(x0 + x, y0 - y, x0 - x, y0 - y, colour);

        SSD1306_DrawLine(x0 + y, y0 + x, x0 - y, y0 + x, colour);
        SSD1306_DrawLine(x0 + y, y0 - x, x0 - y, y0 - x, colour);
    }
}

void SSD1306_On(void){
    ssd1306_WriteCommand(0x8D);
    ssd1306_WriteCommand(0x14);
    ssd1306_WriteCommand(0xAF);
}

void SSD1306_Off(void){
    ssd1306_WriteCommand(0x8D);
    ssd1306_WriteCommand(0x10);
    ssd1306_WriteCommand(0xAE);
}