//
// Created by Csurleny on 5/1/2022.
//

#ifndef INC_11_I2C_MASTER_SSD1306_OLED_H
#define INC_11_I2C_MASTER_SSD1306_OLED_H

#include <stdint.h>
#include "display_fonts.h"

#ifndef SSD1306_I2C_ADDR
#define SSD1306_I2C_ADDR    0x78
#endif

#ifndef SSD1306_WIDTH
#define SSD1306_WIDTH   128
#endif

#ifndef SSD1306_HEIGHT
#define SSD1306_HEIGHT   64
#endif

typedef enum{
    SSD1306_COLOR_BLACK = 0x00,
    SSD1306_COLOR_WHITE = 0x01
}SSD1306_COLOUR_t;

uint8_t SSD1306_Init();
void SSD1306_UpdateScreen();
void SSD1306_ToggleInvert();
void SSD1306_Fill(SSD1306_COLOUR_t colour);
void SSD1306_DrawPixel(uint16_t x, uint16_t y, SSD1306_COLOUR_t colour);
void SSD1306_GotoXY(uint16_t x, uint16_t y);
char SSD1306_Putc(char ch, fDef_t* Font, SSD1306_COLOUR_t colour);
char SSD1306_Puts(char* str, fDef_t* Font, SSD1306_COLOUR_t colour);
void SSD1306_DrawLine(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, SSD1306_COLOUR_t colour);
void SSD1306_DrawRectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, SSD1306_COLOUR_t colour);
void SSD1306_DrawFilledRectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, SSD1306_COLOUR_t colour);
void SSD1306_DrawTriangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t x3, uint16_t y3, SSD1306_COLOUR_t colour);
void SSD1306_DrawFilledTriangle(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t x3, uint16_t y3, SSD1306_COLOUR_t colour);
void SSD1306_DrawCircle(int16_t x0, int16_t y0, int16_t r, SSD1306_COLOUR_t colour);
void SSD1306_DrawFilledCircle(int16_t x0, int16_t y0, int16_t r, SSD1306_COLOUR_t colour);
void SSD1306_On(void);
void SSD1306_Off(void);


#endif //INC_11_I2C_MASTER_SSD1306_OLED_H
